# Diana Bell
# CMSC 331 - Homework 2

list = []

# Save fileas list
file = open('test-data.csv', 'r')
rawData = file.read()
file.close()
data = rawData.split('\n')
for g in data[1:]:
    list.append(g.split(','))


# Counters for statistics
valError = 0
indexError = 0
divZero = 0
numRecords = 1
recordsUsed = -1
tempSum = 0.0


end = len(list) - 1
# Iterate though data
for i in list[1:end]:
    numRecords += 1
    #print(i[2])
    try:
        if len(i) == 4:
            raise ValueError('Missing result!')
        if (int(i[4]) > 0): # only count successes
            recordsUsed += 1
            tempSum += float(i[2])
        for x in i[:3]: # check for empty fields
            if (x == ''): 
                raise IndexError('Field is empty!')
        if (i[3] == ''):
            raise IndexError('Missing player!')
    except ValueError:
        valError += 1
    except IndexError:
        indexError += 1

# Calculate average
try:
    avgDistance = (tempSum/recordsUsed)
except ZeroDivisionError:
    divZero += 1
    avgDistance = 0

# Round to 2 decimal places
avgStr = str(round(avgDistance, 2))

print "Total number of records: ", numRecords
print "The number of records used for calculation: ", recordsUsed
print "The average successful shot distance: ", avgStr
print "Number of index errors: ", indexError
print "Number of value errors: ", valError
print "Number of division by zero errors: ", divZero
