// Diana Bell
// HW5.cpp
// Lexical Analyzer

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <map>
#include <iterator>
 
using namespace std;

//Map for FSM
map<char,map<char,char>> transitions {  {'S',{ {'+','A'} , {'-','A'} , {'0','E'} , {'1','E'} , {'2','E'} , {'3','E'} , {'4','E'} , {'5','E'} , {'6','E'} , {'7','E'} , {'8','E'} , {'9','E'}}} ,
                                        {'A',{ {'+','E'} , {'-','E'} , {'0','B'} , {'1','C'} , {'2','C'} , {'3','C'} , {'4','C'} , {'5','C'} , {'6','C'} , {'7','C'} , {'8','C'} , {'9','C'}}} ,
                                        {'B',{ {'+','E'} , {'-','E'} , {'0','E'} , {'1','D'} , {'2','D'} , {'3','D'} , {'4','D'} , {'5','D'} , {'6','D'} , {'7','D'} , {'8','E'} , {'9','E'}}} ,
                                        {'C',{ {'+','E'} , {'-','E'} , {'0','C'} , {'1','C'} , {'2','C'} , {'3','C'} , {'4','C'} , {'5','C'} , {'6','C'} , {'7','C'} , {'8','C'} , {'9','C'}}} ,
                                        {'D',{ {'+','E'} , {'-','E'} , {'0','D'} , {'1','D'} , {'2','D'} , {'3','D'} , {'4','D'} , {'5','D'} , {'6','D'} , {'7','D'} , {'8','E'} , {'9','E'}}} ,
                                        {'E',{ {'+','E'} , {'-','E'} , {'0','E'} , {'1','E'} , {'2','E'} , {'3','E'} , {'4','E'} , {'5','E'} , {'6','E'} , {'7','E'} , {'8','E'} , {'9','E'}}}};

//Map to track accepted versus rejected states
map<char,string> status { {'S',"Rejected"} , {'A',"Rejected"} , {'B',"Accepted"} , {'C',"Accepted"} , {'D',"Accepted"} , {'E',"Rejected"} };

//Iterators to find values
map<char,char>::iterator it;
map<char,string>::iterator it2;

//Vector to store inputs
vector<string> inputVector;

//Tracks current state. Every input starts at S
char currentState = 'S';


int main() {
    
    //Read file and store in inputVector
    ifstream infile("test-data.txt");
    string input;
    while (infile >> input)
        inputVector.push_back(input);
   
    //Iterate through each input
    for (int i = 0; i < inputVector.size(); i++) {
        //Iterate through each character in the current input
	for (int j = 0; j < inputVector[i].length(); j++) {
            
	    //Find state transition
	    it = transitions[currentState].find(inputVector[i][j]);
            currentState = it->second;
        }
	//Find if the state is accepted or rejected
        it2 = status.find(currentState);
	//Start back at S for next input
	currentState = 'S';
        cout << it2->second << ": " << inputVector[i] << '\n';
    }

    infile.close(); //close file
    return 0;
}
